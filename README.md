# tips-gitlab-ci

Organizing your .gitlab-ci.yml

---

# Cómo organizar tu .gitlab-ci: "extends" &"include"

En ocasiones cuando estamos trabajando en un proyecto durante bastante tiempo, nuestro .gitlab-ci.yml va creciendo sin darnos cuenta. De repente, caemos en la conclusión de que tenemos más de 600 líneas de código y resultan difíciles de leer. Es por ello que en este artículo veremos dos pequeñas claves o "tips" para mejorar la organización de nuestro gitlab-ci.yml.

## División por jobs/stages: el uso "include"
La primera de las claves sería el uso de include, el cual nos va a permitir dividir nuestro .gitlab-ci.yml en varios ficheros distintos. 
Por ejemplo dividiendo por jobs o stages. 

De esta sencilla manera podemos dividir nuestro yaml en diferentes archivos, algo muy útil si estamos configurando un monorepo.

https://docs.gitlab.com/ee/ci/yaml/includes.html#gitlab-cicd-include-examples

---

## Creación de templates: el uso del "extends"
Por otro lado, también podemos hacer uso del extends, algo tremendamente útil si tenemos varios fragmentos de código iguales y lo único que cambia se puede hacer mediante el uso de variables.
Podemos llevar un fragmento de código a otro fichero, por ejemplo, un proceso de build, lo importaríamos con un include en nuestro .gitlab-ci, y ya podríamos hacer uso de este "template" dentro de cualquiera de los ficheros.

https://medium.com/r/?url=https%3A%2F%2Fdocs.gitlab.com%2Fee%2Fci%2Fyaml%2FREADME.html%23extends

## Conclusión
En definitiva, da igual que uses include, extends u otros mecanimos para organizar tu .gitlab-ci, la cuestión es que quede ordenado y mantenible al igual que el resto de nuestro código.
